﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int choiceOperation = 0;
            string choiceContinue = "";
            int choiceBegin = 0;
            double result = 0;
            double firstNumber = 0, secondNumber = 0;
            bool stringComparison = true;

            Console.WriteLine("Welcome to calculator!" + "\n");
            
            NewNumbers:
            Console.WriteLine("You should enter the numbers to proceed (strings or characters are not allowed)." + "\n");

            firstNumber:
            //Ввод чисел, обработка исключений
            try
            {
                Console.WriteLine("Please enter the first number:" + "\n");
                firstNumber = enterNumber();
            }
         
            catch (Exception)
            {
                goto firstNumber;
            }
           
            secondNumber:
            try
            {
                Console.WriteLine("Please enter the second number:" + "\n");
                secondNumber = enterNumber();   
            }

            catch (Exception e)
            {
                goto secondNumber;
            }

            while (true)
            {
                try
                {
                    Console.WriteLine("\n" + "Please choose the operation which you want to provide:" + "\n");
                    Console.WriteLine("1 - Addition of numbers" + "\n" +
                                      "2 - Subtraction of numbers" + "\n" +
                                      "3 - Multiplication of numbers" + "\n" +
                                      "4 - Division of numbers" + "\n");
                    choiceOperation = int.Parse(Console.ReadLine());
                    switch (choiceOperation)
                    {
                        case 1:
                            Console.WriteLine("\n" + "You choosed operation of addition");
                            result = Operation.additionOfNumbers(firstNumber, secondNumber);
                            break;
                        case 2:
                            Console.WriteLine("\n" + "You choosed operation of subtraction");
                            result = Operation.subtractionOfNumbers(firstNumber, secondNumber);
                            break;
                        case 3:
                            Console.WriteLine("\n" + "You choosed operation of multiplication");
                            result = Operation.multiplicationOfNumbers(firstNumber, secondNumber);
                            break;
                        case 4:
                            Console.WriteLine("\n" + "You choosed operation of division");
                            result = Operation.divisionOfNumbers(firstNumber, secondNumber);
                            Console.WriteLine("The division of entered numbers is: " + result);
                            break;
                    }
                }
                catch (DivideByZeroException e)
                {
                    Console.WriteLine(e.Source);
                }
                    Console.WriteLine("\n" + "Press Y if you want to continue or press any character to end the program:");
                    choiceContinue = Console.ReadLine();
                    
                    stringComparison = string.Equals(choiceContinue, "y", StringComparison.OrdinalIgnoreCase);
                    if (stringComparison == true)
                    {
                        Console.WriteLine("\n" + "Do you want to continue with entered numbers or with new:" + "\n" +
                                          "1 - with entered previously" + "\n" +
                                          "2 - with new numbers" + "\n");
                        choiceBegin = int.Parse(Console.ReadLine());
                        Console.WriteLine("\n");
                        switch (choiceBegin)
                        {
                            case 1:
                                break;
                            case 2:
                                goto NewNumbers;
                        }
                        continue;
                    }
                    else
                        break;
               
            }
            Console.WriteLine("\n" + "Thank you for using calculator program. See you next time!");
        }

        public static double enterNumber()
        {
            Regex regs = new Regex(@"^[\+-]?\d*(?:[\.,]\d+)?$");
            string enteredString, trimmedString = "";
            
            enteredString = Console.ReadLine();
            trimmedString = enteredString.Trim();
                
            if (!regs.IsMatch(trimmedString))
            {
                Console.WriteLine("You entered incorrect number. Only digits are allowed. Please try again:");
                throw new Exception();   
            }
            return Double.Parse(trimmedString);
        }
    }
}