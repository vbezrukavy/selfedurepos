﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Operation
    {
        public static double additionOfNumbers(double a, double b)
        {
            double result = a + b;
            Console.WriteLine("The addition of entered numbers is: " + result);
            return result;
        }

        public static double subtractionOfNumbers(double a, double b)
        {
            double result = a - b;
            Console.WriteLine("The subtraction of entered numbers is: " + result);
            return result;
        }

        public static double multiplicationOfNumbers(double a, double b)
        {
            double result = a * b;
            Console.WriteLine("The multiplication of entered numbers is: " + result);
            return result;
        }

        public static double divisionOfNumbers(double a, double b)
        {
            double result = 0;
           
                if (b == 0)
                {
                    throw new DivideByZeroException()
                    {
                        Source = "It is impossible to divide by 0. Please choose another operation for these numbers or return back to enter another numbers"
                    };
                }
                result = a / b;
                return result;
          
        }
    }
}
